package ru.smf;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/greeting")
public class GreetingController {

    @GetMapping
    public String hello() {
        return "Hello Spring";
    }
    @GetMapping("/hello/{s}")
    public ResponseEntity<String> hello2(@PathVariable String s){
        return new ResponseEntity<>(String.format("Hello %s", s), HttpStatus.OK);
    }
}

