package ru.smf;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;


/**
 * @author <a href="mailto:ooovladislavchik@gmail.com>""</a>"
 * Данный класс служит main классом для запуска приложнеия
 * То же что и спринг
 * Используем IoC
 */
@QuarkusMain
public class Main {
    public static void main(String[] args) {
        Quarkus.run(Start.class,args);
    }
    public static class Start implements QuarkusApplication{

        @Override
        public int run(String... args) throws Exception {
            System.out.println("Starting");
            Quarkus.waitForExit();
            return 0;
        }
    }
}
