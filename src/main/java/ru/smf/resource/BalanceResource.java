package ru.smf.resource;

import io.smallrye.mutiny.Uni;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.PATCH;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import ru.smf.domain.Balance;
import ru.smf.service.IBalanceService;

import java.math.BigDecimal;

@Path("balances")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BalanceResource {
    @Inject
    private IBalanceService balanceService;

}
