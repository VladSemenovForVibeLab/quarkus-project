package ru.smf.resource;

import io.smallrye.mutiny.Uni;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import ru.smf.domain.Customer;
import ru.smf.dto.CustomerData;
import ru.smf.dto.CustomerReq;
import ru.smf.service.ICustomerService;

import java.util.List;

@Path("customers")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CustomerResource {
    @Inject
    private ICustomerService customerService;
    @GET
    public Uni<List<CustomerData>> getCustomers(){
        return customerService.getCustomers();
    }
    @POST
    public  Uni<Customer> addCustomer(String name){
        return customerService.addCustomer(name);
    }
    @POST
    @Path("/addCustomer")
    public Uni<Void> createCustomer(CustomerReq customerReq){
        return customerService.createCustomer(customerReq);
    }
    @PUT
    @Path("{id}")
    public Uni<Void> updateCustomer(String id,CustomerReq customerReq){
        return customerService.updateCustomer(id,customerReq);
    }
    @DELETE
    @Path("{id}")
    public Uni<Void> deleteCustomer(String id){
        return customerService.deleteCustomer(id);
    }
}
