package ru.smf.domain;

import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import jakarta.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author <a href="ooovladislavchik@gmail.com"</a>
 * @Entity - аннотация, указывающая, что класс является сущностью, которую можно сохранить в базу данных.
 * public class Balance extends PanacheEntityBase - класс Balance наследуется от PanacheEntityBase, что означает, что этот класс является сущностью, которую мы можем использовать в сочетании с Panache для выполнения операций с базой данных.
 * implements Serializable - класс Balance реализует интерфейс Serializable, что означает, что его объекты могут быть сохранены и восстановлены.
 * Comparable<Balance> - класс Balance реализует интерфейс Comparable с типом Balance, что означает, что объекты класса Balance могут быть сравниваемыми.
 */
@Entity
public class Balance extends PanacheEntityBase implements Serializable, Comparable<Balance> {
    /**
     * @Id - аннотация, указывающая, что поле id является идентификатором сущности.
     * @GeneratedValue(strategy = GenerationType.UUID) - аннотация, указывающая, что значение поля id будет автоматически генерироваться с использованием стратегии UUID.
     * private String id - поле id типа String, которое является идентификатором сущности.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    /**
     * private BigDecimal balance - поле balance типа BigDecimal, которое содержит информацию о балансе.
     */
    private BigDecimal balance;
    /**
     * private LocalDateTime createDate - поле createDate типа LocalDateTime, которое содержит информацию о дате создания баланса.
     */
    private LocalDateTime createDate;
    /**
     * private LocalDateTime updateDate - поле updateDate типа LocalDateTime, которое содержит информацию о дате последнего обновления баланса.
     */
    private LocalDateTime updateDate;
    /**
     * @Transient - аннотация, указывающая, что поле lock не будет сохраняться в базе данных.
     * private Object lock - поле lock типа Object, которое используется для синхронизации доступа к сущности.
     */
    @Transient
    private Object lock = new Object();

    /**
     * @throws Exception
     * @PrePersist - аннотация, указывающая, что метод onCreate будет вызываться перед сохранением сущности в базу данных.
     * public void onCreate() - метод onCreate, который устанавливает значения полей createDate и updateDate перед сохранением сущност
     */
    @PrePersist
    public void onCreate() throws Exception {
        this.createDate = LocalDateTime.now();
        this.updateDate = LocalDateTime.now();
    }

    /**
     * @PreUpdate - аннотация, указывающая, что метод onUpdate будет вызываться перед обновлением сущности в базе данных.
     * public void onUpdate() - метод onUpdate, который устанавливает значение поля updateDate перед обновлением сущности.
     * @throws Exception
     */
    @PreUpdate
    public void onUpdate() throws Exception {
        this.updateDate = LocalDateTime.now();
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Object getLock() {
        return lock;
    }

    public void setLock(Object lock) {
        this.lock = lock;
    }

    private Balance() {
    }


    public Balance(String id, BigDecimal balance) {
        this.id = id;
        this.balance = balance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    /**
     * public int compareTo(Balance o) - переопределенный метод compareTo из интерфейса Comparable, который используется для сравнения объектов класса Balance по полю balance.
     * @param o the object to be compared.
     * @return
     */
    @Override
    public int compareTo(Balance o) {
        return this.balance.compareTo(o.balance);
    }
}
