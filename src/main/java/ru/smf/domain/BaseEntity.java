package ru.smf.domain;

import jakarta.persistence.*;

import java.time.LocalDateTime;

@MappedSuperclass
public class BaseEntity {
    @Id
    private String id;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    @Transient
    private Object lock = new Object();

    @PrePersist
    public void onCreate(){
        this.createDate = LocalDateTime.now();
        this.updateDate = LocalDateTime.now();
    }
    @PreUpdate
    public void onUpdate(){
        this.updateDate = LocalDateTime.now();
    }
}
