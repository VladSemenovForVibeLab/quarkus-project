package ru.smf.service;

import io.smallrye.mutiny.Uni;
import ru.smf.domain.Balance;

import java.math.BigDecimal;

public interface IBalanceService {
    Uni<Balance> addMoneyToBalance(String id, BigDecimal amount);
}
