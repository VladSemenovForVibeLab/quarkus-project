package ru.smf.service.impl;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import ru.smf.domain.Balance;
import ru.smf.service.IBalanceService;

import java.math.BigDecimal;

@ApplicationScoped
public class BalanceServiceImpl implements IBalanceService {
    @Override
    public Uni<Balance> addMoneyToBalance(String id, BigDecimal amount) {
        return Balance.<Balance>findById(id)
                .onItem()
                .ifNotNull().transformToUni(balance->{
                    balance.getBalance().add(amount);
                    return balance.persistAndFlush()
                            .onItem()
                            .transform(__->balance);
                })
                .onItem()
                .ifNull()
                .fail();
    }
}
