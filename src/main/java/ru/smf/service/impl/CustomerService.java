package ru.smf.service.impl;

import io.quarkus.hibernate.reactive.panache.Panache;
import io.quarkus.hibernate.reactive.panache.common.WithSession;
import io.quarkus.hibernate.reactive.panache.common.WithTransaction;
import io.quarkus.logging.Log;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.NotFoundException;
import ru.smf.domain.Balance;
import ru.smf.domain.Customer;
import ru.smf.dto.CustomerData;
import ru.smf.dto.CustomerReq;
import ru.smf.service.ICustomerService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ApplicationScoped
public class CustomerService implements ICustomerService {
    @Inject
    PgPool reactivePgPool;

    /**
     * Create a new customer
     * Данный метод возвращает список объектов CustomerData в виде Reactive Stream (поток), обернутый вниверсальный реактивный тип Uni.
     * Он выполняет следующие действия:
     * 1. Вызывает статический метод findAll() класса Customer, который возвращает Uni<List<Customer>> - реактивный объект, обертывающий список объектов Customer.
     * 2. Вызывает метод list() на реактивном объекте Uni<List<Customer>>, чтобы получить доступ к содержимому списка.
     * 3. Вызывает метод onItem() на реактивном объекте List<Customer>, чтобы выполнить трансформацию (преобразование) содержимого объекта.
     * 4. Внутри блока transform(), каждый объект Customer преобразуется в объект CustomerData с использованием метода map(). Затем, все преобразованные объекты CustomerData объединяются в список с использованием метода toList().
     * 5. В результате, метод возвращает Uni<List<CustomerData>> - реактивный объект, обертывающий список объектов CustomerData.
     * Таким образом, данный метод выполняет операцию получения всех объектов Customer, преобразование каждого из них в объект CustomerData и возвращение списка CustomerData. Каждый объект CustomerData получает нулевое значение для поля BigDecimal.
     *
     * @return
     */
    @Override
    public Uni<List<CustomerData>> getCustomers() {
        return Panache.withSession(() -> Customer.<Customer>findAll()
                .list()
                .onItem()
                .transformToUni((customers) -> {
                    Log.info("Получение баланса!");
                    List<String> customerId = customers.stream()
                            .map(Customer::getId).toList();
                    return Balance.<Balance>find("id in ?1", customerId)
                            .list()
                            .onItem()
                            .transform((balances -> {
                                Map<String, BigDecimal> blanceMap =
                                        balances.stream()
                                                .collect(Collectors.toMap(Balance::getId, Balance::getBalance));
                                Log.info("маппинг баланса!");
                                return customers
                                        .stream()
                                        .map(customer -> new CustomerData(
                                                customer.getId(),
                                                customer.getName(),
                                                blanceMap.getOrDefault(customer.getId(), BigDecimal.ZERO)
                                        ))
                                        .toList();
                            }));
                }));
    }

    // Метод addCustomer(String name) добавляет нового покупателя в базу данных.
// Он создает новый объект Customer с указанным именем, сохраняет его и фиксирует изменения в базе данных, а затем возвращает созданного покупателя.
    @WithTransaction
    @Override
    public Uni<Customer> addCustomer(String name) {
        Customer customer = new Customer(name);
        return customer.persistAndFlush().replaceWith(customer);
    }

    // Метод createCustomer(CustomerReq customerReq) создает нового покупателя на основе запроса, переданного в качестве параметра.
    // Он создает новый объект Customer с указанным именем, сохраняет его и фиксирует изменения в базе данных, а затем возвращает пустой результат.
    @Override
    @WithTransaction
    @WithSession
    public Uni<Void> createCustomer(CustomerReq customerReq) {
        Customer customer = new Customer(customerReq.name());
        return Panache.withSession(() ->
                Panache.withTransaction(() -> {
                    return Customer.persist(customer);
                }));
    }

    // Метод updateCustomer(String id, CustomerReq customerReq) обновляет данные существующего покупателя по указанному идентификатору.
    // Он ищет покупателя по заданному идентификатору, и если покупатель не найден, выбрасывает исключение NotFoundException.
    // Затем он обновляет имя покупателя на основе указанного запроса и сохраняет изменения в базе данных, а затем возвращает пустой результат.
    @Override
    public Uni<Void> updateCustomer(String id, CustomerReq customerReq) {
        return Panache.withSession(() -> Panache.withTransaction(() -> {
            return Customer.<Customer>findById(id)
                    .onItem()
                    .ifNull()
                    .failWith(() -> new NotFoundException("customer not found!"))
                    .onItem()
                    .call((customer) -> {
                        customer.setName(customerReq.name());
                        return Customer.persist(customer);
                    })
                    .replaceWithVoid();
        }));
    }

    // Метод deleteCustomer(String id) удаляет покупателя по указанному идентификатору.
    // Он ищет покупателя по заданному идентификатору, и если покупатель не найден, выбрасывает исключение NotFoundException.
    // Затем он удаляет покупателя из базы данных и возвращает пустой результат.
    @Override
    public Uni<Void> deleteCustomer(String id) {
        return Panache.withSession(() ->
                Customer.<Customer>findById(id)
                        .onItem()
                        .ifNull()
                        .failWith(() -> new NotFoundException("Customer not found!"))
                        .call((customer) -> Customer.deleteById(customer.getId()))
                        .replaceWithVoid());
    }
}
