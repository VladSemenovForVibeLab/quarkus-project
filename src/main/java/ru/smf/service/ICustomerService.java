package ru.smf.service;

import io.quarkus.hibernate.reactive.panache.Panache;
import io.smallrye.mutiny.Uni;
import ru.smf.domain.Customer;
import ru.smf.dto.CustomerData;
import ru.smf.dto.CustomerReq;

import java.util.List;

public interface ICustomerService {
    Uni<List<CustomerData>> getCustomers();

    Uni<Customer> addCustomer(String name);

    Uni<Void> createCustomer(CustomerReq customerReq);
    Uni<Void> updateCustomer(String id,CustomerReq customerReq);
    Uni<Void> deleteCustomer(String id);
}

