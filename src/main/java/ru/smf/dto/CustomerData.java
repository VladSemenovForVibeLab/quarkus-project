package ru.smf.dto;

import java.math.BigDecimal;

public record CustomerData(
        String id,
        String name,
        BigDecimal balance
) {
}
